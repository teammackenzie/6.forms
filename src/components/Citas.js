import React from 'react';
import Badge from './Badge';
import Cita from './Cita';

export default function Citas({ citas, cantidadCitas, eliminarCita }) {
	return (
		<div className="col-md-5 order-md-2 mb-4">
			<Badge cantidadCitas={cantidadCitas} />
			<ul className="list-group mb-3">
				{citas.map(cita => <Cita cita={cita} key={cita.idCita} eliminarCita={eliminarCita} />)}
			</ul>
		</div>
	);
}
