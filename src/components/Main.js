import React, { Component } from 'react';
import Header from './Header';
import Registro from './Registro';
import Citas from './Citas';
import Footer from './Footer';

export default class Main extends Component {
	state = {
		citas: [],
		cantidadCitas: 0
	};

	componentDidMount() {
		const citasLC = localStorage.getItem('citas');
		if (citasLC) {
			this.setState(
				{
					citas: JSON.parse(citasLC)
				},
				() => {
					this.setState({
						cantidadCitas: this.state.citas.length
					});
				}
			);
		}
	}

	componentDidUpdate() {
		localStorage.setItem('citas', JSON.stringify(this.state.citas));
	}

	agregarCita = cita => {
		const citas = [...this.state.citas, cita];
		const cantidadCitas = citas.length;
		this.setState({ citas, cantidadCitas });
	};

	eliminarCita = idCita => {
		const citas = this.state.citas.filter(cita => cita.idCita !== idCita);
		const cantidadCitas = citas.length;
		this.setState({ citas, cantidadCitas });
	};

	render() {
		const { citas, cantidadCitas } = this.state;
		return (
			<div className="container">
				<Header titulo="App Citas" year={new Date().getFullYear()} />
				<div className="row">
					<Registro agregarCita={this.agregarCita} />
					<Citas citas={citas} cantidadCitas={cantidadCitas} eliminarCita={this.eliminarCita} />
				</div>
				<Footer compania="Giovanny Vargas" />
			</div>
		);
	}
}
