import React from 'react';
import './Cita.css';
import PropTypes from 'prop-types';

export default function Cita({ cita, eliminarCita }) {
	const colorCita = cita.prioridad === 'Alta' ? 'bg-alta' : 'bg-media';
	const handleEliminarCita = () => {
		eliminarCita(cita.idCita);
	};
	return (
		<li className={`list-group-item d-flex justify-content-between lh-condensed ${colorCita}`}>
			<div>
				<h6 className="my-0">
					{cita.nombres} {cita.apellidos}
				</h6>
				<small className="text-muted">
					{cita.fecha} - {cita.hora}
				</small>
			</div>

			<span className="text-muted mb-1">{cita.prioridad}</span>
			<button onClick={handleEliminarCita} type="button" className="btn btn-danger btn-sm mb-1">
				X
			</button>
		</li>
	);
}
Cita.propTypes = {
	cita: PropTypes.shape({
		idCita: PropTypes.string.isRequired,
		nombres: PropTypes.string.isRequired
	})
};
