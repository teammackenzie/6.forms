import React from 'react';

export default function Alert() {
	return (
		<div className="alert alert-danger" role="alert">
			Debe ingresar todos los campos!!
		</div>
	);
}
