import React from 'react';

export default function Badge({ cantidadCitas }) {
	return (
		<h4 className="d-flex justify-content-between align-items-center mb-3">
			<span className="text-muted">Citas Programadas</span>
			<span className="badge badge-secondary badge-pill bg-purple text-white lh-100">{cantidadCitas}</span>
		</h4>
	);
}
