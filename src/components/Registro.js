import React, { Component } from 'react';
import Alert from './Alert';
import uuid from 'uuid';
import PropTypes from 'prop-types';

export default class Registro extends Component {
	static propTypes = {
		agregarCita: PropTypes.func.isRequired
	};

	// static defaultProps ={}

	state = {
		validado: false
	};

	//TODO crear Refs
	documentoRef = React.createRef();
	nombresRef = React.createRef();
	apellidosRef = React.createRef();
	fechaRef = React.createRef();
	horaRef = React.createRef();
	epsRef = React.createRef();
	prioridadRef = React.createRef();

	crearCita = event => {
		//TODO crear Object
		event.preventDefault();
		const idCita = uuid(),
			documento = this.documentoRef.current.value,
			nombres = this.nombresRef.current.value,
			apellidos = this.apellidosRef.current.value,
			fecha = this.fechaRef.current.value,
			hora = this.horaRef.current.value,
			eps = this.epsRef.current.value,
			prioridad = this.prioridadRef.current.value;

		if (!eps || !prioridad) {
			this.setState({ validado: true });
		} else {
			this.setState({ validado: false });
			const cita = { idCita, documento, nombres, apellidos, fecha, hora, eps, prioridad };
			//TODO enviar object al padre
			this.props.agregarCita(cita);
			event.currentTarget.reset();
		}
	};

	render() {
		return (
			<div className="col-md-7 order-md-1">
				<h4 className="d-flex justify-content-between align-items-center mb-3">
					<span className="text-muted">Ingreso Cita</span>
				</h4>
				<form onSubmit={this.crearCita}>
					<div className="mb-3">
						<label>Documento</label>
						<input ref={this.documentoRef} type="number" className="form-control" required />
					</div>
					<div className="row">
						<div className="col-md-6 mb-3">
							<label>Nombres</label>
							<input ref={this.nombresRef} type="text" className="form-control" required />
						</div>
						<div className="col-md-6 mb-3">
							<label>Apellidos</label>
							<input ref={this.apellidosRef} type="text" className="form-control" required />
						</div>
					</div>
					<div className="row">
						<div className="col-md-6 mb-3">
							<label>Fecha</label>
							<input ref={this.fechaRef} type="date" className="form-control" />
						</div>
						<div className="col-md-6 mb-3">
							<label>Hora</label>
							<input ref={this.horaRef} type="time" className="form-control" />
						</div>
					</div>
					<div className="row">
						<div className="col-md-6 mb-3">
							<label>Eps</label>
							<select ref={this.epsRef} className="custom-select d-block w-100">
								<option value="">Choose...</option>
								<option value="Sura">Sura</option>
								<option value="Coomeva">Coomeva</option>
								<option value="Medimas">Medimas</option>
							</select>
						</div>
						<div className="col-md-6 mb-3">
							<label>Prioridad</label>
							<select ref={this.prioridadRef} className="custom-select d-block w-100">
								<option value="">Choose...</option>
								<option value="Alta">Alta</option>
								<option value="Media">Media</option>
							</select>
						</div>
					</div>
					{this.state.validado && <Alert />}

					<hr className="mb-4" />
					<button className="btn btn-primary btn-lg btn-block bg-purple text-white lh-100" type="submit">
						Guardar
					</button>
				</form>
			</div>
		);
	}
}
